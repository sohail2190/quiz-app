


const questions = [
    {
        question: "What is the capital city of Pakistan?",
        options: ["Lahore", "Islamabad", "Karachi", "Peshawar"],
        correctAnswer: "Islamabad"
    },
    {
        question: "Which mountain range is located in the northern part of Pakistan and includes some of the world's highest peaks, such as K2?",
        options: ["Himalayas", "Andes", "Alps", "Karakoram"],
        correctAnswer: "Karakoram"
    },
    {
        question: "Pakistan shares its longest border with which country to the east?",
        options: ["Afghanistan", "China", "India", "Iran"],
        correctAnswer: "India"
    },
    {
        question: "What is the national sport of Pakistan?",
        options: ["Cricket", "Hockey", "Football (Soccer)", "Polo"],
        correctAnswer: "Hockey"
    },
    {
        question: "Which river is the longest in Pakistan?",
        options: ["Indus River", "Jhelum River", "Ravi River", "Chenab River"],
        correctAnswer: "Indus River"
    },
    {
        question: "Who is the founder of Pakistan?",
        options: ["Muhammad Ali Jinnah", "Allama Iqbal", "Liaquat Ali Khan", "Ayub Khan"],
        correctAnswer: "Muhammad Ali Jinnah"
    },
    {
        question: "Pakistan gained independence from British rule in what year?",
        options: ["1945", "1946", "1947", "1948"],
        correctAnswer: "1947"
    },
    {
        question: "Which city is known as the 'City of Gardens' in Pakistan?",
        options: ["Karachi", "Lahore", "Islamabad", "Multan"],
        correctAnswer: "Lahore"
    },
    {
        question: "What is the name of the highest plateau in Pakistan, known for its unique ecosystem and wildlife?",
        options: ["Thar Desert", "Cholistan Desert", "Deosai Plains", "Kirthar Range"],
        correctAnswer: "Deosai Plains"
    },
    {
        question: "Which Pakistani physicist is known as the 'Father of the Atomic Bomb' for his role in Pakistan's nuclear weapons program?",
        options: ["Abdul Qadeer Khan", "Abdus Salam", "Pervez Hoodbhoy", "Ishfaq Ahmad"],
        correctAnswer: "Abdul Qadeer Khan"
    }
];
export default questions;