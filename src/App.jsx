import { useState } from 'react'
import './App.css'
import questions from './components/Question'

function App() {

  //----------------------------------------------------------------------
  
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [userAnswer, setUserAnswer] = useState([]);
  const [score, setScore]= useState(0);

  const handleAnswerClick = (selectedOption) => {
    const isCorrect = questions[currentQuestion].correctAnswer === selectedOption;

    setUserAnswer([...userAnswer, selectedOption]);

    if (isCorrect) {
        setScore(score + 1);
    }

    setCurrentQuestion(currentQuestion + 1);
}

  const resetQuestion = () =>{
    setCurrentQuestion(0);
    setUserAnswer([]);
    setScore(0);
  }
//------------------------------------------------------------------------

  return (
    <div className='quize'>
      {
        currentQuestion < questions.length ? ( 
          <div>
            <h2>{questions[currentQuestion].question}</h2>

            <ul>
              {questions[currentQuestion].options.map( (options, index) =>(
                <li onClick={()=>handleAnswerClick(options)}  key={index}>  {options} </li>
              ))}
            </ul>

          </div>
        ):(
          <div className='complete'>
            <h2>Questions completed !</h2>
            <p>Your Score is : <span>{score}/{questions.length} </span></p>
            <button onClick={resetQuestion}>Restart</button>
          </div>
        )
      }
    </div>
  )
}

export default App
